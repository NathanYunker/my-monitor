package my.monitor.app.contorller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import my.monitor.app.model.Food;
import my.monitor.app.services.FoodService;

@RestController
@RequestMapping("/food")
public class FoodController {
	
	@Autowired
    private FoodService foodService;
    
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
    public Food postFood(@RequestBody Food newFood) {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!THE NAME!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println(newFood.getName());
        System.out.println(newFood.getDescription());
        System.out.println(newFood.getCalorieCount());
//        Food foodToSave = new Food();
//        foodToSave.setName(newFood.getName());
//        foodToSave.setDescription(newFood.getDescription());
//        foodToSave.setCalorieCount(newFood.getCalorieCount());
        
       

        Food savedFood = foodService.saveFood(newFood);
//        Food savedFood = FoodService.saveFood(foodToSave);

        return savedFood;
    }
}