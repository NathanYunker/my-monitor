package my.monitor.app.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import my.monitor.app.model.Food;
/**
 *
 * Repository class for the Meal entity
 *
 */
@Repository
public class FoodRepository {

    private static final Logger LOGGER = Logger.getLogger(FoodRepository.class);
 
//    @PersistenceContext
//    EntityManager em;
    
    SessionFactory sessionFactory = new Configuration().configure()
			.buildSessionFactory();



    /**
     * Delete a meal, given its identifier
     *
     * @param deletedMealId - the id of the meal to be deleted
     */
//    public void delete(Long deletedFoodId) {
//        Food delete = em.find(Food.class, deletedFoodId);
//        em.remove(delete);
//    }

    /**
     *
     * finds a meal given its id
     *
     */
//    public Food findFoodByName(String name) {
//    	System.out.println("MADE IT IN HERE!!!!!!!!!!!!!!!!!!!!!!!");
//    	System.out.println(em.find(Food.class, name));
//        return em.find(Food.class, name);
//    }

    /**
     *
     * save changes made to a meal, or create the meal if its a new meal.
     *
     */
    public Food save(Food food) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
 
		Food food2 = new Food(food.getName(), food.getDescription(), food.getCalorieCount());
		session.save(food2);
 
		session.getTransaction().commit();
		session.close();
		return food2;
 
//		session.getTransaction().commit();
//		session.close();
//    	System.out.println("MADE IT IN HERE!!!!!!!!!!!!!!!!!!!!!!!");
//    	System.out.println(food);
//        return em.merge(food);
    }

}
