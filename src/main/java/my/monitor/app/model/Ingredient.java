package my.monitor.app.model;

import java.util.ArrayList;

public class Ingredient {

    private final String name;
    private final ArrayList<String> substitutions;

    public Ingredient(String name, ArrayList<String> substitutions) {
        this.name = name;
        this.substitutions = substitutions;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getSubsitutions() {
        return substitutions;
    }
	
}
