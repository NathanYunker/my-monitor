package my.monitor.app.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="food")
public class Food implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
    private String name;
    private String description;
    private int calorieCount;

    public Food(String name, String description, int calorieCount) {
        this.name = name;
        this.description = description;
        this.calorieCount = calorieCount;
    }
    
    protected Food() {
    }

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public int getCalorieCount() {
        return calorieCount;
    }
    
    public void setCalorieCount(int calorieCount) {
        this.calorieCount = calorieCount;
    }
}
