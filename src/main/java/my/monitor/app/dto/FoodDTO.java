package my.monitor.app.dto;

import my.monitor.app.model.Food;

/**
 *
 * JSON serializable DTO containing Meal data
 *
 */
public class FoodDTO {

	private String name;
    private String description;
    private int calorieCount;

    public FoodDTO() {
    }

    public FoodDTO(String name, String description, int calorieCount) {
        this.name = name;
        this.description = description;
        this.calorieCount = calorieCount;
    }

    public static FoodDTO mapFromFoodEntity(Food food) {
        return new FoodDTO(food.getName(), food.getDescription(), food.getCalorieCount());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCalorieCount() {
        return calorieCount;
    }

    public void setCalorieCount(int calorieCount) {
        this.calorieCount = calorieCount;
    }

}
