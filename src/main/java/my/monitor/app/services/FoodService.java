package my.monitor.app.services;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import my.monitor.app.dao.FoodRepository;
import my.monitor.app.model.Food;

import static my.monitor.app.services.ValidationUtils.assertNotBlank;
import static org.springframework.util.Assert.notNull;

/**
 *
 * Business service for Meal-related operations.
 *
 */
@Service
public class FoodService {

    @Autowired
    FoodRepository foodRepository;

    /**
     *
     * saves a meal (new or not) into the database.
     *
     * @param username - - the currently logged in user
     * @param id - the database ud of the meal
     * @param date - the date the meal took place
     * @param time - the time the meal took place
     * @param description - the description of the meal
     * @param calories - the calories of the meal
     * @return - the new version of the meal
     */

    @Transactional
    public Food saveFood(Food newFood) {

        assertNotBlank(newFood.getName(), "name cannot be blank");
        notNull(newFood.getDescription(), "description is mandatory");
        notNull(newFood.getCalorieCount(), "calorieCount is mandatory");

        Food food = null;
        if (newFood.getName() != null) {
        	System.out.println("Wait !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!S");
        	System.out.println(newFood.getName());
        	food = foodRepository.save(newFood);
            //food = foodRepository.findFoodByName(newFood.getName());
            System.out.println("What !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!S");
        	System.out.println(newFood.getName());

//            food.setDescription(newFood.getDescription());
//            food.setCalorieCount(newFood.getCalorieCount());
        } else {
            System.out.println("Handle this error, FoodServic");
        }

        return food;
    }


}

