package com.springendpoints.spring;

import junit.framework.TestCase;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
 
import my.monitor.app.model.Food;
 
/**
 * Unit test for simple Essen App.
 */
public class AppTest extends TestCase {
 
	public void testApp() {
		SessionFactory sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
 
		Food food2 = new Food("wow", "soo strange", 600);
		session.save(food2);
 
		session.getTransaction().commit();
		session.close();
	}
}